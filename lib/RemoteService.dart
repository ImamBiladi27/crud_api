import 'models/menus.dart';
import 'package:http/http.dart' as http;

class RemoteService {
  Future<List<Menus>?> getPosts() async {
    var client = http.Client();
    var uri = Uri.parse('https://tes-mobile.landa.id/api/menus');
    var response = await client.get(uri);
    if (response.statusCode == 200) {
      var json = response.body;
      return menusFromJson(json);
    }
  }
}
