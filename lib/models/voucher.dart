// To parse this JSON data, do
//
//     final menus = menusFromJson(jsonString);

import 'dart:convert';

Menus menusFromJson(String str) => Menus.fromJson(json.decode(str));

String menusToJson(Menus data) => json.encode(data.toJson());

class Menus {
  int id;
  String kode;
  int nominal;
  DateTime createdAt;
  DateTime updatedAt;

  Menus({
    required this.id,
    required this.kode,
    required this.nominal,
    required this.createdAt,
    required this.updatedAt,
  });

  factory Menus.fromJson(Map<String, dynamic> json) => Menus(
        id: json["id"],
        kode: json["kode"],
        nominal: json["nominal"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "kode": kode,
        "nominal": nominal,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
