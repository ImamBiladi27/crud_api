// To parse this JSON data, do
//
//     final menus = menusFromJson(jsonString);

import 'dart:convert';

List<Menus> menusFromJson(String str) => [
      for (Map<String, dynamic> x in json.decode(str)["datas"])
        Menus.fromJson(x)
    ];

String menusToJson(List<Menus> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Menus {
  int id;
  String nama;
  int harga;
  String tipe;
  String gambar;

  Menus({
    required this.id,
    required this.nama,
    required this.harga,
    required this.tipe,
    required this.gambar,
  });

  factory Menus.fromJson(Map<String, dynamic> json) => Menus(
        id: json["id"],
        nama: json["nama"],
        harga: json["harga"],
        tipe: json["tipe"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nama": nama,
        "harga": harga,
        "tipe": tipe,
        "gambar": gambar,
      };
}
